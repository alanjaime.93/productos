package com.mx.msproducts.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.msproducts.entity.Product;
import com.mx.msproducts.service.IDeliveryService;
import com.mx.msproducts.service.IProductService;
import com.mx.msproducts.util.GenericResponse;

@RestController
@RequestMapping("/products")
@Validated
public class ProductController {
	private IProductService product_service;
	private IDeliveryService delivery_service;

	@Autowired
	public ProductController(IProductService product_service, IDeliveryService delivery_service) {
		this.product_service = product_service;
		this.delivery_service = delivery_service;
	}
	
	@GetMapping("byId/{id}")
	public GenericResponse byId(@PathVariable("id") Long id) {
		delivery_service.getByProductId(id);
		return product_service.getById(id);
	}

	@PostMapping("add")
	public GenericResponse add_product(@Valid @RequestBody  Product product) {
		return product_service.insert(product);
	}

	@PostMapping("update")
	public GenericResponse update_product(@Valid @RequestBody  Product product) {
		return product_service.update(product);
	}

}
