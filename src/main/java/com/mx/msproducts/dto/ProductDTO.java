package com.mx.msproducts.dto;

public class ProductDTO {
	private Long id_product;
    private String name;
    private String category;
    private Long price;
    private String delivery_brand;
    private String delivery_id;
    private String delivery_status;
    public ProductDTO() {
    	
    }
    
	public ProductDTO(Long id_product, String name, String category, Long price, String delivery_brand,
			String delivery_id, String delivery_status) {
		super();
		this.id_product = id_product;
		this.name = name;
		this.category = category;
		this.price = price;
		this.delivery_brand = delivery_brand;
		this.delivery_id = delivery_id;
		this.delivery_status = delivery_status;
	}

	public Long getId_product() {
		return id_product;
	}
	public void setId_product(Long id_product) {
		this.id_product = id_product;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Long getPrice() {
		return price;
	}
	public void setPrice(Long price) {
		this.price = price;
	}
	
	public String getDelivery_brand() {
		return delivery_brand;
	}

	public void setDelivery_brand(String delivery_brand) {
		this.delivery_brand = delivery_brand;
	}

	public String getDelivery_id() {
		return delivery_id;
	}
	public void setDelivery_id(String delivery_id) {
		this.delivery_id = delivery_id;
	}
	public String getDelivery_status() {
		return delivery_status;
	}
	public void setDelivery_status(String delivery_status) {
		this.delivery_status = delivery_status;
	}

	@Override
	public String toString() {
		return "ProductDTO [id_product=" + id_product + ", name=" + name + ", category=" + category + ", price=" + price
				+ ", delivery_brand=" + delivery_brand + ", delivery_id=" + delivery_id + ", delivery_status="
				+ delivery_status + "]";
	}
	
    
}
