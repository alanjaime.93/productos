package com.mx.msproducts.model;


public class Product {
	
    private Long id;

    private String name;

    private String category;
    
    private Long price;
    
    
	public Product() {
		super();
	}

	public Product(Long id, String name, String category, Long price) {
		super();
		this.id = id;
		this.name = name;
		this.category = category;
		this.price = price;
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public Long getPrice() {
		return price;
	}


	public void setPrice(Long price) {
		this.price = price;
	}
   
}
