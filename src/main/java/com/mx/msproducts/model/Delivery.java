package com.mx.msproducts.model;

public class Delivery {
	private Long id;
    private String delivery_brand;
    private String delivery_id;
    private String delivery_status;
	
	public Delivery(Long id, String delivery_brand, String delivery_id, String delivery_status) {
		super();
		this.id = id;
		this.delivery_brand = delivery_brand;
		this.delivery_id = delivery_id;
		this.delivery_status = delivery_status;
	}
	public Delivery() {
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getDelivery_brand() {
		return delivery_brand;
	}
	public void setDelivery_brand(String delivery_brand) {
		this.delivery_brand = delivery_brand;
	}
	public String getDelivery_id() {
		return delivery_id;
	}
	public void setDelivery_id(String delivery_id) {
		this.delivery_id = delivery_id;
	}
	public String getDelivery_status() {
		return delivery_status;
	}
	public void setDelivery_status(String delivery_status) {
		this.delivery_status = delivery_status;
	}
	@Override
	public String toString() {
		return "Delivery [id=" + id + ", delivery_brand=" + delivery_brand + ", delivery_id=" + delivery_id
				+ ", delivery_status=" + delivery_status + "]";
	}
	
	
	
}
