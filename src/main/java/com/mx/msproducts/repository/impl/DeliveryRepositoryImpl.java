package com.mx.msproducts.repository.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.mx.msproducts.model.Delivery;
import com.mx.msproducts.repository.IDeliveryRepository;
import com.mx.msproducts.util.GenericResponse;
import com.mx.msproducts.util.ResponseType;
import java.util.logging.Logger;


@Repository
public class DeliveryRepositoryImpl implements IDeliveryRepository{
	
	@Autowired
	RestTemplate restTemplate;

	@Override
	public GenericResponse insert(Delivery delivery) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericResponse update(Delivery delivery) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Delivery getByProductId(Long id) {
		HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    HttpEntity <String> entity = new HttpEntity<String>(headers);
	    Gson g = new Gson(); 
	    String result = new String();
	    try {
	    	result = restTemplate.exchange("https://retoolapi.dev/7DXe9O/data/"+id, HttpMethod.GET, entity, String.class).getBody();
	    	
		    
	    }
	    catch(HttpClientErrorException e) {
	    	Logger logger
            = Logger.getLogger(DeliveryRepositoryImpl.class.getName());
	    	logger.info("...PRODUCT NOT FOUND..."+e.getStatusCode().toString());
	    }
	    Delivery delivery = g.fromJson(result, Delivery.class);
	    return delivery;
	}

	

}
