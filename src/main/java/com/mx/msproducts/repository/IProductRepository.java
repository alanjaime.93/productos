package com.mx.msproducts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mx.msproducts.entity.Product;

@Repository
public interface IProductRepository extends JpaRepository <Product, Long> { 

}
