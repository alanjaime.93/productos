package com.mx.msproducts.service;

import java.util.List;

import com.mx.msproducts.entity.Product;
import com.mx.msproducts.util.GenericResponse;


public interface IProductService {
	public GenericResponse insert(Product organization);
	public GenericResponse getById(Long id);
	public GenericResponse update(Product product);
}
