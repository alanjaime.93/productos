package com.mx.msproducts.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.msproducts.dto.ProductDTO;
import com.mx.msproducts.entity.Product;
import com.mx.msproducts.model.Delivery;
import com.mx.msproducts.repository.IDeliveryRepository;
import com.mx.msproducts.repository.IProductRepository;
import com.mx.msproducts.repository.impl.DeliveryRepositoryImpl;
import com.mx.msproducts.util.GenericResponse;
import com.mx.msproducts.util.ResponseType;


@Service
public class ProductServiceImpl implements com.mx.msproducts.service.IProductService{
	
	private IProductRepository product_repository;
	private IDeliveryRepository delivery_repository;

    @Autowired
    public ProductServiceImpl(IProductRepository product_repository, IDeliveryRepository delivery_repository) {
		super();
		this.product_repository = product_repository;
		this.delivery_repository = delivery_repository;
	}
	
	@Override
	public GenericResponse insert(Product product) {
		Product newProduct = product_repository.save(product);
		Map<String, Object> numberMapping = new HashMap<>();
		numberMapping.put("Product", newProduct);
		GenericResponse response = new GenericResponse(ResponseType.ACCEPT, numberMapping);
		return response;
	}	
	@Override
	public GenericResponse update(Product product) {
		Optional<Product> prod = product_repository.findById(product.getId());
		Product updatedProd = new Product();
		updatedProd.setId(product.getId());
		updatedProd.setName(product.getName());
		updatedProd.setCategory(product.getCategory());
		updatedProd.setPrice(product.getPrice());
		Map<String, Object> numberMapping = new HashMap<>();
		if(prod.isPresent()) {
			product_repository.save(updatedProd);
			numberMapping.put("Product", updatedProd);
			GenericResponse response = new GenericResponse(ResponseType.ACCEPT, numberMapping);
			return response;
		}
			numberMapping.put("Product", prod.orElse(new Product()));
			GenericResponse response = new GenericResponse(ResponseType.NOT_AVAILABLE, numberMapping);
			return response;
	}


	@Override
	public GenericResponse getById(Long id) {
		Optional<Product> optProd = product_repository.findById(id);
		Delivery delivery = new Delivery();
		ProductDTO productDTO = new ProductDTO();
		delivery.setId((long) 0);
		delivery = delivery_repository.getByProductId(id);
		Map<String, Object> numberMapping = new HashMap<>();
		try {
			if(optProd.isPresent() && !delivery.getId().equals((long) 0)) {
				productDTO.setId_product( optProd.get().getId());
				productDTO.setName(optProd.get().getName());
				productDTO.setCategory(optProd.get().getCategory());
				productDTO.setPrice(optProd.get().getPrice());
				productDTO.setDelivery_status(delivery.getDelivery_status());
				productDTO.setDelivery_brand(delivery.getDelivery_brand());
				productDTO.setDelivery_id(delivery.getDelivery_id());
				numberMapping.put("Product", productDTO);
				GenericResponse response = new GenericResponse(ResponseType.ACCEPT, numberMapping);
				return response;
			}
		}catch(NullPointerException e) {
			Logger logger
	        = Logger.getLogger(DeliveryRepositoryImpl.class.getName());
	    	logger.info("...DELIVERY NOT FOUND..."+e.getMessage());
		}
			
			numberMapping.put("Product", optProd.orElse(new Product()));
			GenericResponse response = new GenericResponse(ResponseType.NOT_AVAILABLE, numberMapping);
			return response;
	       
	}


}
