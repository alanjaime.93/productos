package com.mx.msproducts.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.msproducts.model.Delivery;
import com.mx.msproducts.repository.IDeliveryRepository;
import com.mx.msproducts.service.IDeliveryService;
import com.mx.msproducts.util.GenericResponse;

@Service
public class DeliveryServiceImpl implements IDeliveryService{
	
	private IDeliveryRepository delivery_repository;

    @Autowired
    public DeliveryServiceImpl(IDeliveryRepository delivery_repository) {
		super();
		this.delivery_repository = delivery_repository;
	}

	@Override
	public GenericResponse insert(Delivery delivery) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Delivery getByProductId(Long id) {
		// TODO Auto-generated method stub
		return delivery_repository.getByProductId(id);
	}

	@Override
	public GenericResponse update(Delivery delivery) {
		// TODO Auto-generated method stub
		return null;
	}

}
