package com.mx.msproducts.service;

import com.mx.msproducts.model.Delivery;
import com.mx.msproducts.util.GenericResponse;

public interface IDeliveryService {
	public GenericResponse insert(Delivery delivery);
	public Delivery getByProductId(Long id);
	public GenericResponse update(Delivery delivery);
}
