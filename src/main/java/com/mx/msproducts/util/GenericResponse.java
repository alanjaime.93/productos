package com.mx.msproducts.util;

import java.util.HashMap;
import java.util.Map;


public class GenericResponse {
	private String codeStatus;
    private String message;
    private Map<String,Object> information;


    public GenericResponse() {
    }

    public String getCodeStatus() {
		return codeStatus;
	}

	public void setCodeStatus(String codeStatus) {
		this.codeStatus = codeStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Map<String, Object> getInformation() {
		return information;
	}

	public void setInformation(Map<String, Object> information) {
		this.information = information;
	}

	public GenericResponse(ResponseType type, Map<String,Object> information){
        this.codeStatus = type.getCode();
        this.message = type.getMessage();
        this.information = information;
    }

    public GenericResponse(String codeStatus, String message){
      this.codeStatus = codeStatus;
      this.message = message;
    }

    public GenericResponse(ResponseType type){
      this.codeStatus = type.getCode();
      this.message = type.getMessage();
    }

    public GenericResponse(ResponseType type , String message){
        this.codeStatus = type.getCode();
        this.message = message;
    }

    public GenericResponse(String codeStatus, String message, String key, Object value ){
        this.codeStatus = codeStatus;
        this.message = message;

        if(null == this.information)
            information = new HashMap<>();
        information.put(key,value);
    }

    public GenericResponse(String codeStatus, String message, Map<String, Object> information) {
        this.codeStatus = codeStatus;
        this.message = message;
        this.information = information;
    }
    
    public GenericResponse(String codeStatus,ResponseType type, String key, Object value) {
		this.codeStatus = type.getCode();
        this.message = type.getMessage();
        if (null == this.information)
            information = new HashMap<>();
        information.put(key, value);
	}
	

}
