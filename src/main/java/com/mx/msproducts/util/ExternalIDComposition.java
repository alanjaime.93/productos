package com.mx.msproducts.util;

public class ExternalIDComposition {
	public String get_external_id(String name, long phone, long id ) {
		String name_first_four_char = name.substring(0, 4);
		String string_phone = Long.toString(phone);
		String last_four_digits = string_phone.substring(string_phone.length() - 4);
		String external_id = "NE" + name_first_four_char + last_four_digits + id;
		return external_id;
		
	}
}
