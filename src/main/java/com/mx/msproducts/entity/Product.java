package com.mx.msproducts.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Product{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    private String name;

    @NotEmpty(message = "Category is mandatory")
    @Column(name = "category")
    @Size(message ="category must be max 15 ",max = 15)
    private String category;
    
    @NotNull(message = "Price is mandatory")
    @Column(name = "price")
    @Min(value = 1000, message = "price must have min 4 digits")
    private long price;
    

    public Product() {
	}

    
	public Product(long id, @NotNull String name,
			@NotEmpty(message = "Category is mandatory") @Size(message = "category must be max 15 ", max = 15) String category,
			@NotNull(message = "Price is mandatory") @Min(value = 1000, message = "price must have min 4 digits") long price) {
		super();
		this.id = id;
		this.name = name;
		this.category = category;
		this.price = price;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	
}
